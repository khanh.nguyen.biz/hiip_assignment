from contextlib import contextmanager
from functools import wraps

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from caching import cache

ENV_CONFIG = {
    'DB_USER': 'root',
    'DB_PASS': 'root',
    'DB_HOST': 'localhost',
    'DB_NAME': 'hiip'
}


@cache("GetEnvSqlURI")
def build_env_sql_uri(env_config):
    return 'mysql+pymysql://{DB_USER}:{DB_PASS}@{DB_HOST}/{DB_NAME}?charset=utf8'.format(
        **env_config
    )


@cache("GetSessionDatabaseConnection")
def _get_session_class():
    engine = create_engine(build_env_sql_uri[ENV_CONFIG], connect_args={'options': '-csearch_path=biz_direct'})
    return sessionmaker(
        bind=engine,
        expire_on_commit=False
    )


@contextmanager
def session():
    session_class = _get_session_class()
    _session = session_class()
    try:
        yield _session
    finally:
        _session.close()


def with_session(func):
    @wraps(func)
    def with_session_func_wrapper(*args, **kwargs):
        if 'session' in kwargs:
            return func(*args, **kwargs)

        with session() as sess:
            return func(*args, session=sess, **kwargs)

    return with_session_func_wrapper


@contextmanager
def transaction():
    session_class = _get_session_class()
    _session = session_class()
    try:
        yield _session
        _session.commit()
    except:
        _session.rollback()
        raise
    finally:
        _session.close()


def with_transaction(func):
    @wraps(func)
    def transaction_func_wrapper(*args, **kwargs):
        if 'session' in kwargs:
            return func(*args, **kwargs)

        with transaction() as sess:
            return func(*args, session=sess, **kwargs)

    return transaction_func_wrapper
