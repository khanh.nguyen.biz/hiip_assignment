# encoding=utf-8
import logging


def set_log_level(loggers, level):
    # type: (list[str], int) -> None
    for logger_name in loggers:
        logging.getLogger(logger_name).setLevel(level)


def setup_default_logger(log_format=None, warn=None, info=None):
    if not warn:
        warn = []

    if not info:
        info = []

    return LogBuilder.init(log_format=log_format).\
        warn('botocore', *warn).\
        info('boto3', 's3transfer', 'urllib3', 'awscli', *info)


class LogBuilder(object):
    @staticmethod
    def init(level=logging.DEBUG, format_prefix='', log_format=None):
        if log_format is None:
            log_format = format_prefix + "%(asctime)s %(levelname)s %(name)s %(message)s"

        logging.basicConfig(
            level=level,
            format=log_format,
            datefmt='%H:%M:%S',
            # filename='/tmp/a.log',
        )

        return LogBuilder

    @staticmethod
    def warn(*loggers):
        # type: (list[str]) -> Type[LogBuilder]
        set_log_level(loggers, logging.WARN)

        return LogBuilder

    @staticmethod
    def info(*loggers):
        # type: (list[str]) -> Type[LogBuilder]
        set_log_level(loggers, logging.INFO)

        return LogBuilder

    @staticmethod
    def debug(*loggers):
        # type: (list[str]) -> Type[LogBuilder]
        set_log_level(loggers, logging.DEBUG)

        return LogBuilder


if __name__ == '__main__':
    LogBuilder().info('botocore')
