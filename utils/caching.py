from functools import wraps

_cache = {}


def cache(name):
    def cache_wrapper_level_1(func):
        @wraps(func)
        def cache_wrapper_level_2(*args, **kwargs):
            if name not in _cache:
                _cache[name] = func(*args, **kwargs)

            return _cache[name]

        return cache_wrapper_level_2

    return cache_wrapper_level_1


def clear_cache():
    global _cache
    _cache = {}
