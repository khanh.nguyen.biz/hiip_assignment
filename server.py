from flask import request, jsonify, Flask
from flask_cors import CORS

from apps.max_prime import find_max_prime
import json
import time

app = Flask(__name__)
CORS(app)


@app.route("/")
def main_page():
    return jsonify({
        "success": True,
        "message": "Khanh Nguyen - Interview test"
    })


@app.route("/prime", methods=["GET"])
def prime():
    _input = request.args.get('input')
    start_time = time.time()
    prime_list, result = find_max_prime(int(_input))
    end_time = time.time() - start_time
    return jsonify({
        "success": True,
        "result": {
            "prime_list": list(prime_list),
            "time_execution": round(end_time, 2),
            "number_of_prime": len(list(prime_list)),
            "max_prime": result
        }
    })


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000)
