from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import DateTime, Integer, String
from sqlalchemy.orm import relationship
from datetime import datetime
from core import Base


class InfluencerBank(Base):
    __tablename__ = "InfluencerBank"

    id = Column(
        'id',
        Integer,
        primary_key=True,
    )
    influencer_id = Column(
        'influencer_id',
        Integer,
        ForeignKey("Influencer.influencer_id"),
    )
    bank_id = Column(
        'bank_id',
        Integer,
        ForeignKey("Banks.bank_id"),
    )
    bank_number = Column(
        'bank_number',
        String,
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    Influencer = relationship("Influencer")
    Banks = relationship("Banks")

    def __repr__(self):
        return '<InfluencerBank %r>' % (self.id, )
