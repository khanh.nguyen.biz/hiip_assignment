from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import DateTime, Integer
from sqlalchemy.orm import relationship
from datetime import datetime
from core import Base


class GoogleAccount(Base):
    __tablename__ = "GoogleAccount"

    google_id = Column(
        'google_id',
        Integer,
        primary_key=True,
    )
    influencer_id = Column(
        'influencer_id',
        Integer,
        ForeignKey("Influencer.influencer_id"),
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    Influencer = relationship("Influencer")

    def __repr__(self):
        return '<GoogleAccount %r>' % (self.google_id, )
