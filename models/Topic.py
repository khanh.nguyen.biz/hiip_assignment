from sqlalchemy import Column
from sqlalchemy.types import DateTime, Integer, String
from datetime import datetime
from core import Base


class Topic(Base):
    __tablename__ = "Topic"

    social_network_id = Column(
        'social_network_id',
        Integer,
        primary_key=True,
    )
    name = Column(
        'name',
        String,
    )
    description = Column(
        'description',
        String,
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    def __repr__(self):
        return '<Topic %r>' % (self.social_network_id, )
