from sqlalchemy import Column
from sqlalchemy.types import DateTime, Integer, String
from datetime import datetime
from core import Base


class Influencer(Base):
    __tablename__ = "Influencer"

    influencer_id = Column(
        'influencer_id',
        Integer,
        primary_key=True,
    )
    id_card = Column(
        'id_card',
        Integer,
    )
    first_name = Column(
        'first_name',
        String,
    )
    last_name = Column(
        'last_name',
        String,
    )
    birthday = Column(
        'birthday',
        DateTime,
    )
    gender = Column(
        'gender',
        Integer,
    )
    email = Column(
        'email',
        String,
    )
    address = Column(
        'address',
        String,
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    def __repr__(self):
        return '<Influencer %r>' % (self.influencer_id, )
