from sqlalchemy import Column
from sqlalchemy.types import DateTime, Integer, String
from datetime import datetime
from core import Base


class ExtraInformation(Base):
    __tablename__ = "ExtraInformation"

    info_id = Column(
        'info_id',
        Integer,
        primary_key=True,
    )
    name = Column(
        'name',
        String,
    )
    description = Column(
        'description',
        String,
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    def __repr__(self):
        return '<ExtraInformation %r>' % (self.info_id, )
