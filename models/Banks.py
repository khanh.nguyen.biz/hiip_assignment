from sqlalchemy import Column
from sqlalchemy.types import DateTime, Integer, String
from datetime import datetime
from core import Base


class Banks(Base):
    __tablename__ = "Banks"

    bank_id = Column(
        'bank_id',
        Integer,
        primary_key=True,
    )
    name = Column(
        'name',
        String,
    )
    location = Column(
        'location',
        String,
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    def __repr__(self):
        return '<Banks %r>' % (self.bank_id, )
