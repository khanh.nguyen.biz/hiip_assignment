from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import DateTime, Integer
from sqlalchemy.orm import relationship
from datetime import datetime
from core import Base


class InfluencerExtraInformation(Base):
    __tablename__ = "InfluencerExtraInformation"

    interact_extra_information_id = Column(
        'interact_extra_information_id',
        Integer,
        primary_key=True,
    )
    info_id = Column(
        'info_id',
        Integer,
        ForeignKey("ExtraInformation.info_id"),
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    ExtraInformation = relationship("ExtraInformation")

    def __repr__(self):
        return '<InfluencerExtraInformation %r>' % (
            self.interact_extra_information_id,
        )
