from sqlalchemy import Column
from sqlalchemy.types import DateTime, Integer, String
from datetime import datetime
from core import Base


class InteractTypes(Base):
    __tablename__ = "InteractTypes"

    interact_type_id = Column(
        'interact_type_id',
        Integer,
        primary_key=True,
    )
    name = Column(
        'name',
        String,
    )
    description = Column(
        'description',
        String,
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    def __repr__(self):
        return '<InteractTypes %r>' % (self.interact_type_id, )
