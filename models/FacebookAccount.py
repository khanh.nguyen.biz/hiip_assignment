from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import DateTime, Integer
from sqlalchemy.orm import relationship
from datetime import datetime
from core import Base


class FacebookAccount(Base):
    __tablename__ = "FacebookAccount"

    facebook_id = Column(
        'facebook_id',
        Integer,
        primary_key=True,
    )
    influencer_id = Column(
        'influencer_id',
        Integer,
        ForeignKey("Influencer.influencer_id"),
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    Influencer = relationship("Influencer")

    def __repr__(self):
        return '<FacebookAccount %r>' % (self.facebook_id, )
