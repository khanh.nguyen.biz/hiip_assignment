from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import DateTime, Integer
from sqlalchemy.orm import relationship
from datetime import datetime
from core import Base


class Campaigns(Base):
    __tablename__ = "Campaigns"

    campaign_id = Column(
        'campaign_id',
        Integer,
        primary_key=True,
    )
    influencer_id = Column(
        'influencer_id',
        Integer,
        ForeignKey("Influencer.influencer_id"),
    )
    interact_type_id = Column(
        'interact_type_id',
        Integer,
        ForeignKey("InteractTypes.interact_type_id"),
    )
    number_of_interaction = Column(
        'number_of_interaction',
        Integer,
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    Influencer = relationship("Influencer")
    InteractTypes = relationship("InteractTypes")

    def __repr__(self):
        return '<Campaigns %r>' % (self.campaign_id, )
