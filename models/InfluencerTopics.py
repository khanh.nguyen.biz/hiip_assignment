from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import DateTime, Integer
from sqlalchemy.orm import relationship
from datetime import datetime
from core import Base


class InfluencerTopics(Base):
    __tablename__ = "InfluencerTopics"

    influencer_topic_id = Column(
        'influencer_topic_id',
        Integer,
        primary_key=True,
    )
    influencer_id = Column(
        'influencer_id',
        Integer,
        ForeignKey("Influencer.influencer_id"),
    )
    topic_id = Column(
        'topic_id',
        Integer,
        ForeignKey("Topic.social_network_id"),
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    Influencer = relationship("Influencer")
    Topic = relationship("Topic")

    def __repr__(self):
        return '<InfluencerTopics %r>' % (self.influencer_topic_id, )
