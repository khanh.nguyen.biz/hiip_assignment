from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import DateTime, Integer, String
from sqlalchemy.orm import relationship
from datetime import datetime
from core import Base


class Jobs(Base):
    __tablename__ = "Jobs"

    job_id = Column(
        'job_id',
        Integer,
        primary_key=True,
    )
    influencer_id = Column(
        'influencer_id',
        Integer,
        ForeignKey("Influencer.influencer_id"),
    )
    name = Column(
        'name',
        String,
    )
    description = Column(
        'description',
        String,
    )
    location = Column(
        'location',
        String,
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    Influencer = relationship("Influencer")

    def __repr__(self):
        return '<Jobs %r>' % (self.job_id, )
