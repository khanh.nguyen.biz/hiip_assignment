from sqlalchemy import Column, ForeignKey
from sqlalchemy.types import DateTime, Integer, String
from sqlalchemy.orm import relationship
from datetime import datetime
from core import Base


class InfluencerSocialNetwork(Base):
    __tablename__ = "InfluencerSocialNetwork"

    influencer_social_network_id = Column(
        'influencer_social_network_id',
        Integer,
        primary_key=True,
    )
    influencer_id = Column(
        'influencer_id',
        Integer,
        ForeignKey("Influencer.influencer_id"),
    )
    social_network_id = Column(
        'social_network_id',
        Integer,
        ForeignKey("SocialNetwork.social_network_id"),
    )
    url = Column(
        'url',
        String,
    )
    created_at = Column(
        'created_at',
        DateTime,
        default=datetime.utcnow,
    )
    updated_at = Column(
        'updated_at',
        DateTime,
        default=datetime.utcnow,
    )

    Influencer = relationship("Influencer")
    SocialNetwork = relationship("SocialNetwork")

    def __repr__(self):
        return '<InfluencerSocialNetwork %r>' % (
            self.influencer_social_network_id,
        )
