import unittest
import time
from apps.max_prime import find_max_prime


class TestMaxPrime(unittest.TestCase):
    def test_positive_numbers(self):
        list_primes, actual_max_prime = find_max_prime(117)

        self.assertEqual(3, len(list_primes))
        self.assertEqual([71, 17, 11], list_primes)
        self.assertEqual(71, actual_max_prime)

        list_primes, actual_max_prime = find_max_prime(10101011)
        self.assertEqual(7, len(list_primes))
        self.assertEqual([11000111, 11100101, 10110011, 10111001, 10101101, 10011101, 101111], list_primes)
        self.assertEqual(11100101, actual_max_prime)

    def test_no_prime(self):
        list_primes, actual_max_prime = find_max_prime(444)

        self.assertEqual(0, len(list_primes))
        self.assertEqual(-1, actual_max_prime)

        list_primes, actual_max_prime = find_max_prime(10000)

        self.assertEqual(0, len(list_primes))
        self.assertEqual(-1, actual_max_prime)

    def test_invalid_number(self):
        list_numbers = [-117, 0, 1]
        with self.assertRaises(ValueError):
            for number in list_numbers:
                find_max_prime(number)

    def test_time_execution_simple_number(self):
        simple_numbers = [117, 987, 437]
        # Time execution will be less 0.0002 seconds
        expected_time = 0.0002
        for number in simple_numbers:
            start_time = time.time()
            find_max_prime(number)
            time_execution = time.time() - start_time

            self.assertLess(time_execution, expected_time)

    def test_time_execution_complex_number(self):
        complex_numbers = [12520793, 98563748, 56476873]
        # Time execution will be less 0.5 seconds
        expected_time = 0.5
        for number in complex_numbers:
            start_time = time.time()
            find_max_prime(number)
            time_execution = time.time() - start_time

            self.assertLess(time_execution, expected_time)


if __name__ == '__main__':
    unittest.main()
