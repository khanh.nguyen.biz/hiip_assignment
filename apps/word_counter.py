from __future__ import print_function
import sys
from operator import add

from pyspark import SparkConf, SparkContext


def word_counter():
    ret = {}
    if len(sys.argv) != 2:
        print("Usage: wordcount <file>", file=sys.stderr)
        sys.exit(-1)

    conf = SparkConf().setAppName("Word Counter")
    sc = SparkContext(conf=conf)
    lines = sc.textFile(sys.argv[1]).flatMap(lambda line: line.split(' ')).map(
        lambda word: word.replace(',', '').replace('.', '')).filter(lambda word: word.isalpha())
    counts = lines.flatMap(lambda x: x.split(' ')) \
        .map(lambda x: (x, 1)) \
        .reduceByKey(add)
    output = counts.collect()
    for (word, count) in output:
        ret[word.encode('utf-8')] = count

    sc.stop()
    return ret


if __name__ == '__main__':
    word_counter()