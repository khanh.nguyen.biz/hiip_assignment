import random
from itertools import permutations


def power(x, y, p):
    """Big Mod"""
    res = 1
    x = x % p

    while y > 0:
        if y & 1:
            res = (res * x) % p
        y = y >> 1  # y = y/2
        x = (x * x) % p

    return res


def miiller_test(d, n):
    """
    This function is called  for all k trials. It returns
    false if n is composite and returns true if n is probably prime. d is an odd
    number such that d*2<sup>r</sup> = n-1 for some r >= 1
    """
    a = 2 + random.randint(1, n - 4)
    x = power(a, d, n)

    if x == 1 or x == n - 1:
        return True
    while d != n - 1:
        x = (x * x) % n
        d *= 2

        if x == 1:
            return False
        if x == n - 1:
            return True

    return False


def is_prime(n, k):
    """
    It returns false if n is composite and returns true if n is probably prime. k is an
    input parameter that determines accuracy level. Higher value of k indicates more accuracy.
    """
    if n <= 1 or n == 4:
        return False
    if n <= 3:
        return True

    d = n - 1
    while d % 2 == 0:
        d //= 2

    for i in range(k):
        if not miiller_test(d, n):
            return False

    return True


def number_to_list(n):
    number_list = []
    while n:
        number_list.append(n % 10)
        n //= 10
    return number_list


def list_to_number(array):
    ret = 0
    for i in range(len(array)):
        ret += array[-i - 1] * (10 ** i)
    return ret


def find_max_prime(n):
    if n <= 0 or n == 1:
        raise ValueError("Please enter integer number greater than 1")

    max_prime = -1
    number_list = number_to_list(n)
    list_primes = []

    for i in range(len(number_list), 0, -1):
        permutation_list = list(permutations(number_list, i))
        for p in permutation_list:
            number = list_to_number(p)

            if is_prime(number, 6) and number not in list_primes:
                list_primes.append(number)
                if number > max_prime:
                    max_prime = number

        if max_prime > -1:
            break
    return list_primes, max_prime

