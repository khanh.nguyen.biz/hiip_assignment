from datetime import datetime

from models.Influencer import Influencer
from models.InfluencerTopics import InfluencerTopics
from models.Topic import Topic
from utils.database_connector import with_session

_PAGE_SIZE = 20
GENDER_MAPPING = {
    "female": 0,
    "male": 1,
}


@with_session
def get_influencers(session, page=1, from_date=None):
    def _build_query(base_query):
        if from_date:
            base_query = base_query.filter(Influencer.created_at == datetime.strptime(from_date, '%Y-%m-%d'))
        else:
            base_query = base_query.order_by(Influencer.updated_at.desc())

        return base_query

    influencer_query = session.query(Influencer)
    influencers = _build_query(influencer_query).offset((page - 1) * _PAGE_SIZE).limit(_PAGE_SIZE).all()

    return influencers


@with_session
def search_influencer_info(session, field, value, page=1):
    _get_column_name = getattr(Influencer, field)

    return session.query(Influencer).filter(_get_column_name == value).offset((page - 1) * _PAGE_SIZE).limit(
        _PAGE_SIZE).all()


@with_session
def influencers_report(session):
    total_influencers = session.query(Influencer.influencer_id).count()
    _count_female_influencers = session.query(Influencer.influencer_id).filter(
        Influencer.gender == GENDER_MAPPING["female"]).count()
    _count_male_influencers = session.query(Influencer.influencer_id).filter(
        Influencer.gender == GENDER_MAPPING["male"]).count()

    def _ratio_influencers_gender(total, number_of_gender):
        return (float(number_of_gender) / float(total)) * 100

    return {
        "total": total_influencers,
        "female_ratio": _ratio_influencers_gender(total_influencers, _count_female_influencers),
        "male_ratio": _ratio_influencers_gender(total_influencers, _count_male_influencers)
    }


@with_session
def topic_report(session):
    number_of_influencer_topics = session.query(InfluencerTopics.influencer_topic_id).count()

    influencer_topic = session.query(Influencer.influencer_id, InfluencerTopics.influencer_topic_id).join(Topic,
                                                                                                          Influencer).count()

    return number_of_influencer_topics, influencer_topic