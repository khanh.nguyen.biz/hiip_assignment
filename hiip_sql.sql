-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jun 15, 2019 at 02:46 PM
-- Server version: 5.7.22
-- PHP Version: 7.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hiip`
--

-- --------------------------------------------------------

--
-- Table structure for table `Banks`
--

CREATE TABLE `Banks` (
  `bank_id` int(10) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Campaigns`
--

CREATE TABLE `Campaigns` (
  `campaign_id` int(10) NOT NULL,
  `influencer_id` int(11) NOT NULL,
  `interact_type_id` int(11) NOT NULL,
  `number_of_interaction` int(10) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ExtraInformation`
--

CREATE TABLE `ExtraInformation` (
  `info_id` int(10) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `FacebookAccount`
--

CREATE TABLE `FacebookAccount` (
  `facebook_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `influencer_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `GoogleAccount`
--

CREATE TABLE `GoogleAccount` (
  `google_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `influencer_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Influencer`
--

CREATE TABLE `Influencer` (
  `influencer_id` int(10) NOT NULL,
  `id_card` int(10) NOT NULL,
  `first_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `share_topic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avg_interact` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_profile` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `InfluencerBank`
--

CREATE TABLE `InfluencerBank` (
  `id` int(10) NOT NULL,
  `bank_id` int(10) NOT NULL,
  `influencer_id` int(10) NOT NULL,
  `bank_number` int(15) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `InfluencerExtraInformation`
--

CREATE TABLE `InfluencerExtraInformation` (
  `interact_extra_information_id` int(10) NOT NULL,
  `info_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `InfluencerSocialNetwork`
--

CREATE TABLE `InfluencerSocialNetwork` (
  `influencer_social_network_id` int(10) NOT NULL,
  `influencer_id` int(10) NOT NULL,
  `social_network_id` int(11) NOT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `InfluencerStatus`
--

CREATE TABLE `InfluencerStatus` (
  `status_id` int(10) NOT NULL,
  `influencer_id` int(10) NOT NULL,
  `name` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `InfluencerTopics`
--

CREATE TABLE `InfluencerTopics` (
  `influencer_topic_id` int(10) NOT NULL,
  `influencer_id` int(10) NOT NULL,
  `topic_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `InstagramAccount`
--

CREATE TABLE `InstagramAccount` (
  `instagram_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `influencer_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `InteractTypes`
--

CREATE TABLE `InteractTypes` (
  `interact_type_id` int(10) NOT NULL,
  `name` int(11) DEFAULT NULL,
  `description` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Jobs`
--

CREATE TABLE `Jobs` (
  `job_id` int(10) NOT NULL,
  `influencer_id` int(10) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `SocialNetwork`
--

CREATE TABLE `SocialNetwork` (
  `social_network_id` int(10) NOT NULL,
  `name` int(11) DEFAULT NULL,
  `description` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Topic`
--

CREATE TABLE `Topic` (
  `topic_id` int(10) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `TwitterAccount`
--

CREATE TABLE `TwitterAccount` (
  `twitter_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `influencer_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Banks`
--
ALTER TABLE `Banks`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `Campaigns`
--
ALTER TABLE `Campaigns`
  ADD PRIMARY KEY (`campaign_id`),
  ADD KEY `influencer_id` (`influencer_id`),
  ADD KEY `interact_type_id` (`interact_type_id`);

--
-- Indexes for table `ExtraInformation`
--
ALTER TABLE `ExtraInformation`
  ADD PRIMARY KEY (`info_id`);

--
-- Indexes for table `FacebookAccount`
--
ALTER TABLE `FacebookAccount`
  ADD PRIMARY KEY (`facebook_id`),
  ADD KEY `influencer_id` (`influencer_id`);

--
-- Indexes for table `GoogleAccount`
--
ALTER TABLE `GoogleAccount`
  ADD KEY `influencer_id` (`influencer_id`);

--
-- Indexes for table `Influencer`
--
ALTER TABLE `Influencer`
  ADD PRIMARY KEY (`influencer_id`);

--
-- Indexes for table `InfluencerBank`
--
ALTER TABLE `InfluencerBank`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bank_id` (`bank_id`),
  ADD KEY `influencer_id` (`influencer_id`);

--
-- Indexes for table `InfluencerExtraInformation`
--
ALTER TABLE `InfluencerExtraInformation`
  ADD PRIMARY KEY (`interact_extra_information_id`),
  ADD KEY `info_id` (`info_id`);

--
-- Indexes for table `InfluencerSocialNetwork`
--
ALTER TABLE `InfluencerSocialNetwork`
  ADD PRIMARY KEY (`influencer_social_network_id`),
  ADD KEY `influencer_id` (`influencer_id`),
  ADD KEY `social_network_id` (`social_network_id`);

--
-- Indexes for table `InfluencerStatus`
--
ALTER TABLE `InfluencerStatus`
  ADD PRIMARY KEY (`status_id`),
  ADD KEY `influencer_id` (`influencer_id`);

--
-- Indexes for table `InfluencerTopics`
--
ALTER TABLE `InfluencerTopics`
  ADD PRIMARY KEY (`influencer_topic_id`),
  ADD KEY `topic_id` (`topic_id`),
  ADD KEY `influencer_id` (`influencer_id`);

--
-- Indexes for table `InstagramAccount`
--
ALTER TABLE `InstagramAccount`
  ADD PRIMARY KEY (`instagram_id`),
  ADD KEY `influencer_id` (`influencer_id`);

--
-- Indexes for table `InteractTypes`
--
ALTER TABLE `InteractTypes`
  ADD PRIMARY KEY (`interact_type_id`);

--
-- Indexes for table `Jobs`
--
ALTER TABLE `Jobs`
  ADD PRIMARY KEY (`job_id`),
  ADD KEY `influencer_id` (`influencer_id`);

--
-- Indexes for table `SocialNetwork`
--
ALTER TABLE `SocialNetwork`
  ADD PRIMARY KEY (`social_network_id`);

--
-- Indexes for table `Topic`
--
ALTER TABLE `Topic`
  ADD PRIMARY KEY (`topic_id`);

--
-- Indexes for table `TwitterAccount`
--
ALTER TABLE `TwitterAccount`
  ADD KEY `influencer_id` (`influencer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Banks`
--
ALTER TABLE `Banks`
  MODIFY `bank_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Campaigns`
--
ALTER TABLE `Campaigns`
  MODIFY `campaign_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ExtraInformation`
--
ALTER TABLE `ExtraInformation`
  MODIFY `info_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `InfluencerBank`
--
ALTER TABLE `InfluencerBank`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `InfluencerExtraInformation`
--
ALTER TABLE `InfluencerExtraInformation`
  MODIFY `interact_extra_information_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `InfluencerSocialNetwork`
--
ALTER TABLE `InfluencerSocialNetwork`
  MODIFY `influencer_social_network_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `InfluencerStatus`
--
ALTER TABLE `InfluencerStatus`
  MODIFY `status_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `InfluencerTopics`
--
ALTER TABLE `InfluencerTopics`
  MODIFY `influencer_topic_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `InteractTypes`
--
ALTER TABLE `InteractTypes`
  MODIFY `interact_type_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Jobs`
--
ALTER TABLE `Jobs`
  MODIFY `job_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `SocialNetwork`
--
ALTER TABLE `SocialNetwork`
  MODIFY `social_network_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Topic`
--
ALTER TABLE `Topic`
  MODIFY `topic_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Campaigns`
--
ALTER TABLE `Campaigns`
  ADD CONSTRAINT `Campaigns_ibfk_1` FOREIGN KEY (`influencer_id`) REFERENCES `Influencer` (`influencer_id`),
  ADD CONSTRAINT `Campaigns_ibfk_2` FOREIGN KEY (`interact_type_id`) REFERENCES `InteractTypes` (`interact_type_id`);

--
-- Constraints for table `FacebookAccount`
--
ALTER TABLE `FacebookAccount`
  ADD CONSTRAINT `FacebookAccount_ibfk_1` FOREIGN KEY (`influencer_id`) REFERENCES `Influencer` (`influencer_id`);

--
-- Constraints for table `GoogleAccount`
--
ALTER TABLE `GoogleAccount`
  ADD CONSTRAINT `GoogleAccount_ibfk_1` FOREIGN KEY (`influencer_id`) REFERENCES `Influencer` (`influencer_id`);

--
-- Constraints for table `InfluencerBank`
--
ALTER TABLE `InfluencerBank`
  ADD CONSTRAINT `InfluencerBank_ibfk_1` FOREIGN KEY (`bank_id`) REFERENCES `Banks` (`bank_id`),
  ADD CONSTRAINT `InfluencerBank_ibfk_2` FOREIGN KEY (`influencer_id`) REFERENCES `Influencer` (`influencer_id`);

--
-- Constraints for table `InfluencerExtraInformation`
--
ALTER TABLE `InfluencerExtraInformation`
  ADD CONSTRAINT `InfluencerExtraInformation_ibfk_1` FOREIGN KEY (`info_id`) REFERENCES `ExtraInformation` (`info_id`);

--
-- Constraints for table `InfluencerSocialNetwork`
--
ALTER TABLE `InfluencerSocialNetwork`
  ADD CONSTRAINT `InfluencerSocialNetwork_ibfk_1` FOREIGN KEY (`influencer_id`) REFERENCES `Influencer` (`influencer_id`),
  ADD CONSTRAINT `InfluencerSocialNetwork_ibfk_2` FOREIGN KEY (`social_network_id`) REFERENCES `SocialNetwork` (`social_network_id`);

--
-- Constraints for table `InfluencerStatus`
--
ALTER TABLE `InfluencerStatus`
  ADD CONSTRAINT `InfluencerStatus_ibfk_1` FOREIGN KEY (`influencer_id`) REFERENCES `Influencer` (`influencer_id`);

--
-- Constraints for table `InfluencerTopics`
--
ALTER TABLE `InfluencerTopics`
  ADD CONSTRAINT `InfluencerTopics_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `Topic` (`topic_id`),
  ADD CONSTRAINT `InfluencerTopics_ibfk_2` FOREIGN KEY (`influencer_id`) REFERENCES `Influencer` (`influencer_id`);

--
-- Constraints for table `InstagramAccount`
--
ALTER TABLE `InstagramAccount`
  ADD CONSTRAINT `InstagramAccount_ibfk_1` FOREIGN KEY (`influencer_id`) REFERENCES `Influencer` (`influencer_id`);

--
-- Constraints for table `Jobs`
--
ALTER TABLE `Jobs`
  ADD CONSTRAINT `Jobs_ibfk_1` FOREIGN KEY (`influencer_id`) REFERENCES `Influencer` (`influencer_id`);

--
-- Constraints for table `TwitterAccount`
--
ALTER TABLE `TwitterAccount`
  ADD CONSTRAINT `TwitterAccount_ibfk_1` FOREIGN KEY (`influencer_id`) REFERENCES `Influencer` (`influencer_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
