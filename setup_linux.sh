#!/usr/bin/env bash

set -e

USER=`whoami`

echo "Hi $USER!"

if [ $USER == "root" ]; then
    echo "This script should not be run as root."
    exit 1
fi

# Install docker
INSTALL_DOCKER=0
if [ ! -e /usr/bin/docker ]; then
    INSTALL_DOCKER=1
    echo Setup docker...
    curl -sSL https://get.docker.com/ | sh

    sudo groupadd docker || true

    sudo usermod -aG docker $USER

    # Install docker-compose
    sudo curl -sSL https://github.com/docker/compose/releases/download/1.8.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose

    sudo ln -sf /usr/bin/docker /usr/bin/d
    sudo ln -sf /usr/local/bin/docker-compose /usr/bin/dc
fi

# Setup virtualenv
if [ ! -e /usr/bin/virtualenv ] && [ ! -e /usr/local/bin/virtualenv ]; then
    echo Install virtualenv...
    sudo apt-get install -qqy python-virtualenv
fi

# Install lib for mysql
echo Setup lib for mysql
sudo apt-get install -qqy libpq-dev libmysqlclient-dev python-dev build-essential swig libpulse-dev mysql-client

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
$DIR/setup

echo Done!

if [ $INSTALL_DOCKER == 1 ]; then
  echo Please logout and login again to make the docker install work!.
fi
